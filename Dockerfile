FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

# Mandatory dependencies
RUN apt update && apt upgrade -y
RUN apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    openssh-client \
    wget

# Prepare SSH dir
RUN mkdir -p ~/.ssh
RUN chmod 700 ~/.ssh

# PHP dependencies
RUN add-apt-repository -y ppa:ondrej/php
RUN apt update && apt upgrade -y
RUN apt install -y \
    php7.4 \
    php7.4-xml \
    php7.4-mbstring \
    php7.4-tokenizer \
    php7.4-gd \
    php7.4-zip \
    php7.4-mysql \
    php7.4-pgsql \
    php7.4-bcmath \
    composer

# Node dependencies
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt install -y nodejs
RUN npm i -g yarn

# Go dependencies
RUN wget https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.13.3.linux-amd64.tar.gz
RUN rm go1.13.3.linux-amd64.tar.gz
RUN export PATH="/usr/local/go/bin:$PATH"

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"

# AWS dependencies
RUN apt install -y python3-pip
RUN pip3 install awscli --upgrade

# Clean up
RUN apt clean
RUN apt autoremove -y

# Show up
RUN php -v
RUN node -v
RUN aws --version
RUN go version
